﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//
using Moq;
//
using FactoryMaster.Domain.API;
using FactoryMaster.Domain.Core;

namespace FactoryMasterUnitTests {
   [TestClass]
   public class ModuleTests {

      [TestMethod]
      public void InitializeModule ( ) {
         var expCapacity = 1000d;
         var tankModule = new Mock<Module>( ).Object;

         Assert.IsNotNull(tankModule, "module did not initialize");
      }

   }
}
