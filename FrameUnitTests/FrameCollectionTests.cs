﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using FactoryMaster.Domain.API;
using FactoryMaster.Domain.Core;

namespace FactoryMasterUnitTests {
   [TestClass]
   public class FrameCollectionTests {

      [TestMethod]
      public void ConstructFrameCollection ( ) {
         var frames = Factories.Collections.Frames();
         Assert.IsNotNull(frames, "FrameCollection failed to initialize.");
      }

      [TestMethod]
      public void AddFrame ( ) {
         var frames = Factories.Collections.Frames( );

         var actFrame = default(Frame);
         var isInstalled = frames.Add(StructureFacing.Top, out actFrame);

         Assert.IsTrue(isInstalled, "frame not installed");
      }

      [TestMethod]
      public void GetFacingFrame ( ) {
         var expFrameCount = 1;
         var frames = Factories.Collections.Frames(StructureFacing.Top);
         var frame = default(Frame);

         Assert.AreEqual(expFrameCount, frames.Count, string.Format("actual frame count: {0}", frames.Count));
         Assert.IsTrue(frames.TryGetFacingFrame(StructureFacing.Top, out frame), "frame did not exist");
      }

      [TestMethod]
      public void AttemptAddFrameToPreassignedFacing ( ) {
         var frames = Factories.Collections.Frames(StructureFacing.Top);

         var actFrame = default(Frame);
         var isInstalled = frames.Add(StructureFacing.Top, out actFrame);

         Assert.IsFalse(isInstalled, "frame installed");
      }
   }
}
