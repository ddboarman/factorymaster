﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//
using Moq;
//
using FactoryMasterUnitTests.TestObjects;
//
using FactoryMaster.Domain.API;
using FactoryMaster.Domain.Core;

namespace FactoryMasterUnitTests {
   [TestClass]
   public class FunctionalPipeTests : Builder {
      [TestMethod]
      public void ConstructGenericPipe( ) {
         // supplying StructureFacing args should establish 2 Frames, 1 on Left & 1 on Right
         // if no frame is associated with the supplied StructureFacing, then FacingFrame() returns Frame.Empty
         var pipe = new GenericStructure(StructureFacing.Left, StructureFacing.Right);
         Assert.AreEqual(Frame.Empty, pipe.FacingFrame(StructureFacing.Top), "oops!!! should have a null frame here");

         // get facing frames
         var lFrame = pipe.FacingFrame(StructureFacing.Left);
         Assert.IsNotNull(lFrame, "left frame not available");

         var rFrame = pipe.FacingFrame(StructureFacing.Right);
         Assert.IsNotNull(lFrame, "right frame not available");
         // ************************************

         // install Input and Output Adapter(s)
         Assert.IsTrue(lFrame.Install(new GenericAdapter(AdapterDirection.Input)), "missing left frame input adapter");
         Assert.IsTrue(rFrame.Install(new GenericAdapter(AdapterDirection.Output)), "missing right frame output adapter");
         // ************************************

         // get Input and Output Adapter(s)
         var inputs = lFrame.Inputs();
         Assert.IsTrue(inputs.Length == 1, "left frame input adapter count mismatch");

         var outputs = rFrame.Outputs();
         Assert.IsTrue(outputs.Length == 1, "right frame output adapter count mismatch");

         var input = inputs[0];
         var output = outputs[0];
         // ************************************

         // attach input and output adapters
         Assert.IsTrue(input.AttachAdapter(output), "input and output failed to attach");

         // pipe completed!!!
      }

      [TestMethod]
      public void ConstructConfigureElbowPipe( ) {
         // using StructureExtensions with fluent API to configure frame and adapter install for an elbow pipe
         var pipe = new Pipe(StructureFacing.Front, StructureFacing.Right)
            .Configure(p => p.FacingFrame(StructureFacing.Front), f => f.Install(new InputAdapter()))
            .Configure(p => p.FacingFrame(StructureFacing.Right), f => f.Install(new OutputAdapter()));

         // not fond of the index parameter
         var isAttached = pipe.FacingFrame(StructureFacing.Front).Inputs()[0]
            .AttachAdapter(pipe.FacingFrame(StructureFacing.Right).Outputs()[0]);

         Assert.IsTrue(isAttached, "input and output failed to attach");

         // pipe completed!!!
      }

      [TestMethod]
      public void PipeConstructionWithTankModule( ) {
         var pipe = new Pipe();
         var tank = pipe.CreateModule(() => new TankModule())
            .Configure(t => t.Max = 10)
            .Configure(t => t.Level = 0);

         var iFrame = pipe.InstallFrame(StructureFacing.Left, ( ) => Factories.Frames.New());
         var oFrame = pipe.InstallFrame(StructureFacing.Right, ( ) => Factories.Frames.New());

         var input = iFrame.Create(( ) => new InputAdapter())
            .Configure(i => {
               var limiter = i.CreateModule(( ) => new CapacityModule())
                  .Configure(c => c.Max = .0125d);
               i.CreateModule(( ) => new FlowModule(limiter));

               return i;
            });

         var output = oFrame.Create(( ) => new OutputAdapter())
            .Configure(o => {
               var limiter = o.CreateModule(( ) => new CapacityModule())
                  .Configure(c => c.Max = .0125d);
               o.CreateModule(( ) => new FlowModule(limiter));

               return o;
            });

         tank.Connect(t => t.Input, input);
         tank.Connect(t => t.Output, output);

         Assert.IsTrue(tank.Input.IsAttached(input), "tank module input not connected");
         Assert.IsTrue(tank.Output.IsAttached(output), "tank module output not connected");
      }

      [TestMethod]
      public void FillPipeOnTick( ) {
         var pipe = BuildPipe();
         var resource = BuildResourceConstruct();
         var source = resource.Source;
         var pipeInput = pipe.FacingFrame(StructureFacing.Left).Inputs()[0];

         pipeInput.AttachAdapter(source);

         pipe.Tick();

         Thread.Sleep(25);

         var tank = pipe.GetModule<TankModule>();
         Assert.IsTrue(tank.Level == .00125, "tank did not receive fluid");
      }
   }
}
