﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using FactoryMaster.Domain.API;
using FactoryMaster.Domain.Core;

namespace FactoryMasterUnitTests {
   [TestClass]
   public class FrameTests {

      [TestMethod]
      public void ConstructFrame ( ) {
         var frame = new Frame( );
         Assert.IsNotNull(frame, "Frame failed to initialize.");
      }

      [TestMethod]
      public void GetDefaultMaxFrameAdapters ( ) {
         var expMaxAdapters = 1;
         var frame = new Frame( );

         Assert.AreEqual(expMaxAdapters, frame.MaxComponents, string.Format("actual max frames: {0}", frame.MaxComponents));
      }

      [TestMethod]
      public void SetMaxFrameAdapters ( ) {
         var expMaxAdapters = 2;
         var frame = new Frame( )
            .SetMaxComponents(expMaxAdapters);

         Assert.AreEqual(expMaxAdapters, frame.MaxComponents, string.Format("actual max frames: {0}", frame.MaxComponents));
      }

      [TestMethod]
      public void FrameSerialNumber ( ) {
         var frame = Factories.Frames.New( );

         Assert.IsNotNull(frame.SerialNumber);
         Console.WriteLine("Frame SerialNumber: {0}", frame.SerialNumber);
      }

      [TestMethod]
      public void FrameInstallAdapter() {
         var frame = Factories.Frames.New();
         var adapter = new Mock<Adapter>(new object[] { AdapterDirection.Input }).Object;
         
         Assert.IsTrue(frame.Install(adapter), "adapter not installed");
      }

      [TestMethod]
      public void FrameInstallAdapterFalse() {
         var frame = Factories.Frames.New();
         var adapter = new Mock<Adapter>(new object[] { AdapterDirection.Input }).Object;

         Assert.IsTrue(frame.Install(adapter), "adapter not installed");
         // since frame has max 1 adapter slot, installing second adapter will be false
         Assert.IsFalse(frame.Install(adapter), "adapter installed");
      }

   }
}
