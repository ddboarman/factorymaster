﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//
using Moq;
//
using FactoryMaster.Domain.Core;
using FactoryMaster.Domain.API;

namespace FactoryMasterUnitTests {
   [TestClass]
   public class ConstructTests : Builder {

      [TestMethod]
      public void InitializeMockConstruct ( ) {
         var mockConstruct = new Mock<Construct>(new object[] { new StructureFacing[] { StructureFacing.Top } });

         Assert.IsNotNull(mockConstruct.Object, "construct failed to initialize");
         Assert.IsInstanceOfType(mockConstruct.Object, typeof(Construct), "construct is not correct type");
      }

      [TestMethod]
      public void EnhanceConstructCapacity ( ) {
         var mockConstruct = new Mock<Construct>(new object[] { new StructureFacing[] { StructureFacing.Virtual } }).Object;

         // adding a CapacityModule to the mock construct
         var expModule = mockConstruct.CreateModule(( ) => new CapacityModule( ))
            // and configuring max capacity
            .Configure(c => c.Max = 10000);

         // retrieve the module from mock construct
         var actModule = mockConstruct.GetModule<CapacityModule>( );

         Assert.IsInstanceOfType(actModule, typeof(CapacityModule), "wrong module type");
         Assert.AreEqual(expModule.Name, actModule.Name, "names are not the same");
      }

      [TestMethod]
      public void AddMultipleConstructModules ( ) {
         var expName = "Reservoir";
         var mockConstruct = new Mock<Construct>(new object[] { new StructureFacing[] { StructureFacing.Virtual } }).Object;

         var capModule = mockConstruct.CreateModule(( ) => new CapacityModule( ))
            .Configure(c => c.Max = 10000);

         // make room for another module
         var count = mockConstruct.ModuleCount;
         mockConstruct.SetMaxModules(++count);
         Assert.AreEqual(count, mockConstruct.MaxModules, "module limit counts not equal");

         // adding a ResourceModule to the mock construct
         var resModule = mockConstruct.CreateModule(( ) => new ResourceModule(expName));

         Assert.IsNotNull(resModule, "resource module was not added");
      }

      [TestMethod]
      public void AddMultipleConstructModulesFail ( ) {
         var expName = "Reservoir";
         var mockConstruct = new Mock<Construct>(new object[] { new StructureFacing[] { StructureFacing.Virtual } }).Object;

         var capModule = mockConstruct.CreateModule(( ) => new CapacityModule( ))
            .Configure(c => c.Max = 10000);

         // adding a ResourceModule to the mock construct - no module slots available
         var resModule = mockConstruct.CreateModule(( ) => new ResourceModule(expName));

         Assert.IsNull(resModule, "resource module was added");
      }

      [TestMethod]
      public void EnhanceConstructResource ( ) {
         var expName = "Reservoir";
         var mockConstruct = new Mock<Construct>(new object[] { new StructureFacing[] { StructureFacing.Virtual } }).Object;
         mockConstruct.SetMaxModules(2);

         var capModule = mockConstruct.CreateModule(( ) => new CapacityModule( ))
            .Configure(c => c.Max = 1000);

         // adding a ResourceModule to the mock construct
         var resModule = mockConstruct.CreateModule(( ) => new ResourceModule(expName))
            // configure resource level based on capacity, with auto fill
            .Configure(r => r.Max = 1000)
            .Configure(r => r.Level = 1000)
            .Configure(r => r.AutoFill = true);

         Assert.AreEqual(capModule.Max, resModule.Max, "max levels are not equal");
         Assert.AreEqual(capModule.Max, resModule.Level, "resource did not fill");
      }

      [TestMethod]
      public void InitializeResourceConstruct ( ) {
         var mockConstruct = new Mock<ResourceConstruct>(new object[] {100, false }).Object;

         Assert.IsNotNull(mockConstruct, "resource construct did not initialize");
      }

      [TestMethod]
      public void ResourceConstructSource ( ) {
         var mockConstruct = new Mock<ResourceConstruct>(new object[] {100, false }).Object;
         var source = mockConstruct.Source;

         Assert.IsNotNull(source, "resource construct source is null");
      }

      [TestMethod]
      public void ResourceConstructSourceDefaultDirection ( ) {
         var mockConstruct = new Mock<ResourceConstruct>(new object[] {100, false }).Object;
         var source = mockConstruct.Source;

         Assert.AreEqual(source.Direction, AdapterDirection.Virtual, "source adapter direction is not virtual");
      }

      [TestMethod]
      public void ResourceConstructSourceAttachPipe ( ) {
         var mockConstruct = new Mock<ResourceConstruct>(new object[] {100, false }).Object;
         var source = mockConstruct.Source;

         var pipe = new Pipe(StructureFacing.Bottom, StructureFacing.Top);
         var pipeInput = pipe.FacingFrame(StructureFacing.Bottom)
            .Create(() => new InputAdapter( ))
            .Configure(i => {
               var module = i.CreateModule(( ) => new CapacityModule( ))
                  .Configure(c => c.Max = .00125);

               return i;
            });

         if (pipeInput == null) {
            Assert.Fail("input was not installed");
         }
         
         pipeInput.AttachAdapter(source);
         Assert.IsTrue(pipeInput.IsAttached(source));
      }

      [TestMethod]
      public void ResourceConstructSourceCapacitySetOnAttach ( ) {
         var mockConstruct = new Mock<ResourceConstruct>(new object[] {10000, false }).Object;
         var source = mockConstruct.Source;

         var pipe = BuildPipe();
         var pipeInput = pipe.FacingFrame(StructureFacing.Left).Inputs()[0];

         pipeInput.AttachAdapter(source);
         Assert.IsTrue(pipeInput.IsAttached(source));

         var pipeFlowModule = pipeInput.GetModule<FlowModule>();
         var sourceFlowModule = source.GetModule<FlowModule>();

         Assert.AreEqual(pipeFlowModule.Rate, sourceFlowModule.Rate, "flow rates are not equal");
      }
   }
}
