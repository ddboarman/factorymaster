﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using FactoryMaster.Domain.API;
using FactoryMaster.Domain.Core;

namespace FactoryMasterUnitTests {
   [TestClass]
   public class AdapterTests {

      [TestMethod]
      public void AdapterFrameNull( ) {
         var input = new InputAdapter();
         Assert.IsNull(input.Frame, "adapter frame is not null");
      }

      [TestMethod]
      public void AdapterFrameInstall( ) {
         var frame = Factories.Frames.New();
         var input = new InputAdapter();

         Assert.IsTrue(frame.Install(input), "input adapter not installed in frame");
         Assert.IsNotNull(input.Frame, "adapter frame not set");
      }

      [TestMethod]
      public void AdapterFrameCreate( ) {
         var frame = Factories.Frames.New();
         // frame.Create installs the new component
         var input = frame.Create(() => new InputAdapter());

         Assert.IsNotNull(input.Frame, "adapter frame not set");
      }

      [TestMethod]
      public void InputAdapterCapacityModule ( ) {
         var expCapacity = .00125d;
         var input = new InputAdapter( )
            .Configure(i => {
               var module = i.CreateModule(( ) => new CapacityModule( ))
                  .Configure(c => c.Max = expCapacity);

               return i;
            });

         var capModule = input.GetModule<CapacityModule>( );

         Assert.IsNotNull(capModule, "capacity module not installed");
         Assert.AreEqual(expCapacity, capModule.Max, "capacities are not equal");
      }

      [TestMethod]
      public void AdapterRaiseAsyncStateChangedEvent ( ) {
         var input = new InputAdapter( );
         var output = new OutputAdapter( );
         
         var isConnected = false;
         // configure state changed event handler
         input.StateChanged += (oldState, newState) => {
            isConnected = newState == ConnectionState.Connected;
            Assert.AreEqual(input.State, ConnectionState.Connected, "input not connected");
         };
         // now attach adapters
         input.AttachAdapter(output);

         while (!isConnected) {
            Thread.Sleep(1);
         }

         Assert.IsTrue(isConnected, "adapter attach event not raised");
         Assert.IsTrue(input.IsAttached(output), "input and output not connected");
      }

      [TestMethod]
      public void AdapterRaiseAttachEvent ( ) {
         var input = new InputAdapter( );
         var output = new OutputAdapter( );
         
         var isAttached = false;
         // configure attached event handler
         input.Attached += (i, o) => {
            isAttached = i.IsAttached(output);
            Assert.AreEqual(output, o, "output and o are not identical");
         };
         // now attach adapters
         input.AttachAdapter(output);

         Assert.IsTrue(isAttached, "adapter attach event not raised");
      }

      [TestMethod]
      public void AdapterFlowRateAndFactor ( ) {
         var input = new InputAdapter( );

         // configure mock input limiter - e.g. CapacityModule
         var mockInLimiter = new Mock<ILimiter>( );
         mockInLimiter.Setup(iL => iL.Max)
            .Returns(.00125d);

         var inFlow = input.CreateModule(( ) => new FlowModule(mockInLimiter.Object))
            .Configure(f => f.Rate = .00125d);

         Assert.AreEqual(1, inFlow.Factor, "input flow factor != 1");
      }

      [TestMethod]
      public void AdapterConnectFlowRate ( ) {
         var input = new InputAdapter( );    // small
         var output = new OutputAdapter( );  // large

         // configure mock input limiter - e.g. CapacityModule
         var mockInLimiter = new Mock<ILimiter>( );
         mockInLimiter.Setup(iL => iL.Max)
            .Returns(.00125d);
         // configure mock output limiter
         var mockOutLimiter = new Mock<ILimiter>( );
         mockOutLimiter.Setup(oL => oL.Max)
            .Returns(.0025d);

         var outFlow = output.CreateModule(( ) => new FlowModule(mockOutLimiter.Object))
            .Configure(f => f.Rate = .0025d);   // moves .0025 units per tick

         var inFlow = input.CreateModule(( ) => new FlowModule(mockInLimiter.Object))
            .Configure(f => f.Rate = .00125d);  // moves .00125 units per tick

         input.AttachAdapter(output);

         Assert.AreEqual(1, outFlow.Factor, "output flow factor != 1");
         Assert.AreEqual(2, inFlow.Factor, "input flow factor != 2");
      }

   }
}
