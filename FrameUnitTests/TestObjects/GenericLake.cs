﻿using System;

using FactoryMaster.Domain.Core;

namespace FactoryMasterUnitTests.TestObjects {
   internal class GenericLake : Construct {
      private CapacityModule capacity;


      public GenericLake ( ) : base(new StructureFacing[] { StructureFacing.Top }) {
         capacity = CreateModule(( ) => new CapacityModule( ))
            .Configure(c => c.Max = 10000);
      }


      /// <summary>
      /// 
      /// </summary>
      public override void Tick( ) {
         throw new NotImplementedException();
      }

   }
}
