﻿using System;

using FactoryMaster.Domain.Core;

namespace FactoryMasterUnitTests.TestObjects {
   internal class GenericStructure : Structure {

      public GenericStructure(params StructureFacing[] Facings) : base(Facings) { }


      /// <summary>
      /// 
      /// </summary>
      public override void Tick( ) {
         throw new NotImplementedException();
      }

   }
}
