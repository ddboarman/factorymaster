﻿using System;
//
using FactoryMaster.Domain.API;
using FactoryMaster.Domain.Core;

namespace FactoryMasterUnitTests.TestObjects {
   internal class GenericAdapter : Adapter {

      public GenericAdapter(AdapterDirection Direction) : base(Direction) { }


      /// <summary>
      /// 
      /// </summary>
      /// <returns></returns>
      public override ISource GetSource( ) {
         throw new NotImplementedException();
      }

      /// <summary>
      /// 
      /// </summary>
      public override void Tick( ) {
         throw new NotImplementedException();
      }

   }
}
