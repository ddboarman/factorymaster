﻿using System;
//
using Moq;
//
using FactoryMaster.Domain.API;
using FactoryMaster.Domain.Core;

namespace FactoryMasterUnitTests {
   public class Builder {

      /// <summary>
      /// Build default pipe construct
      /// <para>Left: Input (.0125)</para>
      /// <para>Right: Output (.0125)</para>
      /// <para>Tank: 10</para>
      /// </summary>
      /// <returns>Pipe</returns>
      public Pipe BuildPipe( ) {
         var pipe = new Pipe();
         var tank = pipe.CreateModule(( ) => new TankModule())
            .Configure(t => t.Max = 10)
            .Configure(t => t.Level = 0);

         var iFrame = pipe.InstallFrame(StructureFacing.Left, ( ) => Factories.Frames.New());
         var oFrame = pipe.InstallFrame(StructureFacing.Right, ( ) => Factories.Frames.New());

         var input = iFrame.Create(( ) => new InputAdapter())
            .Configure(i => {
               var limiter = i.CreateModule(( ) => new CapacityModule())
                  .Configure(c => c.Max = .0125d);
               i.CreateModule(( ) => new FlowModule(limiter));

               return i;
            });

         var output = oFrame.Create(( ) => new OutputAdapter())
            .Configure(o => {
               var limiter = o.CreateModule(( ) => new CapacityModule())
                  .Configure(c => c.Max = .0125d);
               o.CreateModule(( ) => new FlowModule(limiter));

               return o;
            });

         tank.Connect(t => t.Input, input);
         tank.Connect(t => t.Output, output);

         return pipe;
      }

      /// <summary>
      /// Build default resource reservoir
      /// <para>Capacity: 10000</para>
      /// </summary>
      /// <returns>ResourceConstruct</returns>
      public ResourceConstruct BuildResourceConstruct( ) {
         return new Mock<ResourceConstruct>(new object[] { 10000, false }).Object; ;
      }
   }
}
