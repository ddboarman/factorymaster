﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using FactoryMasterUnitTests.TestObjects;

using FactoryMaster.Domain.Core;
using FactoryMaster.Domain.API;

namespace FactoryMasterUnitTests {
   [TestClass]
   public class StructureTests {

      [TestMethod]
      public void ConstructMockStructure() {
         var mockStructure = new Mock<Structure>();
         Assert.IsNotNull(mockStructure);
      }

      [TestMethod]
      public void StructureSerialNumber() {
         var structure = new Mock<Structure>().Object;

         Assert.IsNotNull(structure.SerialNumber, "serial number is null");
         Console.WriteLine("Structure SerialNumber: {0}", structure.SerialNumber);
      }

      [TestMethod]
      public void GetFacingFrame() {
         var structure = new Mock<Structure>().Object;
         var frame = structure.FacingFrame(StructureFacing.Top);

         Assert.AreNotEqual(Frame.Empty, frame, "frame not installed on Top");
      }

      [TestMethod]
      public void GetFacingFrameNull() {
         var structure = new Mock<Structure>().Object;
         var frame = structure.FacingFrame(StructureFacing.Rear);

         Assert.AreEqual(Frame.Empty, frame, "frame not Empty on Rear");
      }

      [TestMethod]
      public void GetFrameFacing() {
         var expFacing = StructureFacing.Top;

         var structure = new Mock<Structure>().Object;
         var frame = structure.FacingFrame(expFacing);

         var actFacing = structure.FrameFacing(frame);

         Assert.AreEqual(expFacing, actFacing, "facings are not equal");
      }

      [TestMethod]
      public void GetFrameFacingUnknown() {
         var expFacing = StructureFacing.Unknown;

         var structure = new Mock<Structure>().Object;
         var frame = Factories.Frames.New();

         Assert.AreEqual(expFacing, structure.FrameFacing(frame), "facings are not equal");
      }

      [TestMethod]
      public void InstallFrame() {

      }
   }
}
