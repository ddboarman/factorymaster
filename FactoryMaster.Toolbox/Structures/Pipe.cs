﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace FactoryMaster.Domain.Core {
   public class Pipe : Structure{

      /// <summary>
      /// Constructs the default pipe structure with Left & Right frames
      /// </summary>
      public Pipe ( ) : this(StructureFacing.Left, StructureFacing.Right) { }

      /// <summary>
      /// Constructs the pipe structure with the specified facing frames
      /// </summary>
      /// <param name="Facing">Requires at least one Facing</param>
      /// <param name="Facings">Specified facing frames, requires at least one</param>
      public Pipe (StructureFacing Facing, params StructureFacing[] Facings) : base(ConcatFacings(Facing, Facings)) { }


      /// <summary>
      /// 
      /// </summary>
      public override void Tick( ) {
         foreach (var frame in frames) {
            foreach (var input in frame.Inputs()) {
               Task.Factory.StartNew(input.Tick);
            }
         }
      }


      /// <summary>
      /// concatenates facings into single array
      /// </summary>
      /// <param name="Facing"></param>
      /// <param name="Facings"></param>
      /// <returns></returns>
      private static StructureFacing[] ConcatFacings (StructureFacing Facing, params StructureFacing[] Facings) {
         var facings = new List<StructureFacing>( );
         facings.Add(Facing);
         facings.AddRange(Facings);

         return facings.ToArray();
      }

   }
}
