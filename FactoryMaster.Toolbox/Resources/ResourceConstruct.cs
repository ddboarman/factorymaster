﻿using System;
using System.Linq;

namespace FactoryMaster.Domain.Core {
   public abstract class ResourceConstruct : Construct {

      private CapacityModule capModule;
      private ResourceModule resModule;
      private FlowModule floModule;

      private Adapter sourceAdapter;

      /// <summary>
      /// Get resource construct source
      /// </summary>
      public Adapter Source {
         get {
            return sourceAdapter;
         }
      }


      /// <summary>
      /// Initialize resource construct
      /// </summary>
      /// <param name="AutoFill">Whether resource construct is continually auto filled</param>
      public ResourceConstruct (double MaxCapacity, bool AutoFill = false)
         : base(new StructureFacing[] { StructureFacing.Virtual }, 3) {
         if (!FacingFrame(StructureFacing.Virtual)
            .Install(new VirtualAdapter( ), out sourceAdapter)) {
            // problem
         }

         resModule = CreateModule(( ) => new ResourceModule( ))
            .Configure(r => r.AutoFill = AutoFill)
            .Configure(r => r.Max = MaxCapacity);

         capModule = Source.CreateModule(( ) => new CapacityModule( ))
            .Configure(c => c.Max = resModule.Max);
         floModule = Source.CreateModule(( ) => new FlowModule(capModule));

         sourceAdapter.Attached += sourceAdapterAttached;
      }


      /// <summary>
      /// 
      /// </summary>
      /// <param name="Adapter"></param>
      /// <param name="AttachedAdapter"></param>
      private void sourceAdapterAttached(Adapter Adapter, Adapter AttachedAdapter) {
         var outFloModule = AttachedAdapter.GetModule<FlowModule>();
         if (outFloModule != null) {
            floModule.MatchRate(outFloModule);
         }
      }

   }
}
