﻿using System;
//
using FactoryMaster.Domain.API;

namespace FactoryMaster.Domain.Core {
   public class OutputAdapter : Adapter {

      public OutputAdapter ( ) : base(AdapterDirection.Output) { }


      public override void Tick( ) {
         throw new NotImplementedException();
      }


      public override ISource GetSource( ) {
         return GetModule<TankModule>();
      }
   }
}
