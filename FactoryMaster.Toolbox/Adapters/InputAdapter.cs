﻿using System;
using System.Linq;
//
using FactoryMaster.Domain.API;

namespace FactoryMaster.Domain.Core {
   public class InputAdapter : Adapter {

      private TankModule tank;
      private FlowModule regulator;

      /// <summary>
      /// 
      /// </summary>
      public InputAdapter( ) : base(AdapterDirection.Input) {
         Attached += InputAttached;

      }


      /// <summary>
      /// 
      /// </summary>
      /// <returns></returns>
      public override ISource GetSource( ) {
         throw new NotImplementedException();
      }

      /// <summary>
      /// 
      /// </summary>
      public override void Tick( ) {
         if (tank == null) {
            tank = GetModule<TankModule>();
         }
         if (regulator == null) {
            regulator = GetModule<FlowModule>();
         }
         if (tank == null || regulator == null) {
            throw new InvalidOperationException("EXCEPTION: missing either TankModule or FlowModule");
         }

         tank.Level += regulator.Rate;

         foreach (var output in attached.Where(a => a is OutputAdapter)) {
            output.Source.Level -= regulator.Rate;
         }
      }


      /// <summary>
      /// 
      /// </summary>
      /// <param name="Adapter"></param>
      /// <param name="AttachedAdapter"></param>
      private void InputAttached(Adapter Adapter, Adapter AttachedAdapter) {
         var flow = GetModule<FlowModule>();
         var output = AttachedAdapter as OutputAdapter;

         if (flow == null) {
            return;
         }

         if (output != null) {
            var outFlow = output.GetModule<FlowModule>();
            if (outFlow == null) {
               flow.Rate = 0;
            }
            else {
               flow.Rate = outFlow.Rate;
            }
         }
      }

   }
}
