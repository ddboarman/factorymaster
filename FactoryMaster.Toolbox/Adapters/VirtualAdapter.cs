﻿using System;
using FactoryMaster.Domain.API;

namespace FactoryMaster.Domain.Core {
   public class VirtualAdapter : Adapter {

      private double defaultCapacity = Double.NaN;
      private CapacityModule capacityModule;


      /// <summary>
      /// 
      /// </summary>
      /// <param name="Direction"></param>
      public VirtualAdapter (AdapterDirection Direction = AdapterDirection.Virtual) : base(Direction) {
         capacityModule = this.CreateModule(( ) => new CapacityModule( ))
            .Configure(c => c.Max = defaultCapacity);
      }


      /// <summary>
      /// 
      /// </summary>
      /// <returns></returns>
      public override ISource GetSource( ) {
         throw new NotImplementedException();
      }

      /// <summary>
      /// 
      /// </summary>
      public override void Tick( ) {
         throw new NotImplementedException();
      }


      /// <summary>
      /// 
      /// </summary>
      /// <param name="Connection"></param>
      internal void Connect(Adapter Connection) {
         throw new NotImplementedException();
      }
   }
}
