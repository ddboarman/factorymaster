﻿using System;
using System.Linq.Expressions;
using System.Reflection;
//
using FactoryMaster.Domain.API;

namespace FactoryMaster.Domain.Core {
   public class TankModule : ConfigurableModule<TankModule>, IReservoir, ISource {

      /// <summary>
      /// 
      /// </summary>
      public VirtualAdapter Input { get; private set; }

      /// <summary>
      /// 
      /// </summary>
      public VirtualAdapter Output { get; private set; }

      
      /// <summary>
      /// 
      /// </summary>
      public TankModule( ) : this("Capacity") { }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="Name"></param>
      public TankModule(string ModuleName) {
         Name = ModuleName;
      }


      /// <summary>
      /// 
      /// </summary>
      /// <param name="VirtualAdapter"></param>
      /// <param name="Connection"></param>
      public void Connect(Expression<Func<TankModule, VirtualAdapter>> VirtualAdapter, Adapter Connection) {
         var adapterExpr = VirtualAdapter.Body as MemberExpression;
         if (adapterExpr != null) {
            var adapterSet = adapterExpr.Member as PropertyInfo;
            if (adapterSet != null) {
               var virtualAdapter = new VirtualAdapter();
               virtualAdapter.AttachAdapter(Connection);

               adapterSet.SetValue(this, virtualAdapter, null);
            }
         }
      }

      /// <summary>
      /// 
      /// </summary>
      /// <typeparam name="TValue"></typeparam>
      /// <param name="Set"></param>
      /// <returns></returns>
      public override TankModule Configure<TValue>(Func<TankModule, TValue> Set) {
         Set(this);

         return this;
      }


      #region ILimiter Members

      /// <summary>
      /// Get current resource level (remaining)
      /// </summary>
      public double Level { get; set; }

      /// <summary>
      /// 
      /// </summary>
      public double Max { get; set; }

      #endregion
   }
}
