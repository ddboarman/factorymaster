﻿using System;

using FactoryMaster.Domain.API;

namespace FactoryMaster.Domain.Core {
   public class ResourceModule : ConfigurableModule<ResourceModule>, IReservoir {
      
      /// <summary>
      /// Get or set whether the resource is continually refilled
      /// </summary>
      public bool AutoFill { get; set; }
      
      /// <summary>
      /// Get maximum resource level value
      /// </summary>
      public double Max { get; set; }

      /// <summary>
      /// Get current resource level (remaining)
      /// </summary>
      public double Level { get; set; }
      
      /// <summary>
      /// Initialize a resource module
      /// </summary>
      public ResourceModule( ) : this("Resource") { }

      /// <summary>
      /// Initialize a resource module
      /// </summary>
      /// <param name="Name">ModuleName</param>
      public ResourceModule (string ModuleName) {
         Name = ModuleName;
      }

      /// <summary>
      /// 
      /// </summary>
      /// <typeparam name="TValue"></typeparam>
      /// <param name="Set"></param>
      /// <returns></returns>
      public override ResourceModule Configure<TValue> (Func<ResourceModule, TValue> Set) {
         Set(this);

         return this;
      }

   }
}
