﻿using System;
using FactoryMaster.Domain.API;

namespace FactoryMaster.Domain.Core {
   /// <summary>
   /// Flow module provides a rate of movement per tick. Encapsulates a capacity module.
   /// </summary>
   public class FlowModule : ConfigurableModule<FlowModule> {

      private ILimiter limiter;
      private double matchCapacity;
      
      /// <summary>
      /// Get or set flow per tick
      /// </summary>
      public double Rate { get; set; }

      /// <summary>
      /// Get the flow rate modifier: default is 1
      /// </summary>
      public double Factor {
         get { return 1 / (matchCapacity / Rate); }
      }


      /// <summary>
      /// 
      /// </summary>
      public FlowModule (ILimiter Limiter) : this(Limiter, "Flow") {
         /* greatly simplified, non-compressible fluid must move the same volume out that is moved into the structure:
          * 
          * The smaller structure has an input rate of .00125 by default. 
          * When attached to a larger output with a rate of .0025 (per tick), the smaller input must keep up. The
          * capacity cannot change, but the rate of flow can.
          * 
          * Working formula: Rf = Ro / Ri
          * Rf is Rate Factor
          * Ro is Rate Out
          * Ri is Rate In
          * 
          * given simplified flow formula above: .0025 / .00125 = 2
          * while structureA is attached to structureB, structureA's Ri is modified by factor of 2
          * since structureB's Ro is twice that of structureA's input.
          * ***/
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="ModuleName"></param>
      public FlowModule (ILimiter Limiter, string ModuleName) {
         Name = ModuleName;
         limiter = Limiter;
         matchCapacity = limiter.Max;
         Rate = limiter.Max;
      }


      /// <summary>
      /// 
      /// </summary>
      /// <typeparam name="TValue"></typeparam>
      /// <param name="Set"></param>
      /// <returns></returns>
      public override FlowModule Configure<TValue> (Func<FlowModule, TValue> Set) {
         Set(this);

         return this;
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="outFloModule"></param>
      public void MatchRate(FlowModule outFloModule) {
         matchCapacity = outFloModule.limiter.Max;
         Rate = outFloModule.Rate;
      }

   }
}
