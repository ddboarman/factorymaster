﻿using System;

using FactoryMaster.Domain.API;

namespace FactoryMaster.Domain.Core {
   public class CapacityModule : ConfigurableModule<CapacityModule>, ILimiter {
      
      /// <summary>
      /// 
      /// </summary>
      public CapacityModule( ) : this("Capacity") { }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="Name"></param>
      public CapacityModule(string ModuleName) {
         Name = ModuleName;
      }


      /// <summary>
      /// 
      /// </summary>
      /// <typeparam name="TValue"></typeparam>
      /// <param name="Set"></param>
      /// <returns></returns>
      public override CapacityModule Configure<TValue>(Func<CapacityModule, TValue> Set) {
         Set(this);

         return this;
      }


      #region ILimiter Members
      /// <summary>
      /// Get maximum capacity value
      /// </summary>
      public double Max { get; set; }

      #endregion
   }
}
