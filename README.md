# README #

Install Visual Studio. Download source and compile in VS.

### What is this repository for? ###

* Factory Master
* 0.0.1

### What is Factory Master? ###

Intended to be a game wherein a player designs machines. A machine has 1 or more interfaces (or ~~Conveyors~~ Adapters) through which materials - wood, stone, oil, water, etc. - are either input or output. Machines may process wheat into flour, or oil into gas or other products. Connect machine interfaces together to build complex systems.

There is no User Interface (GUI) yet, but one is planned and will be designed in WPF on a Hexagon-based map.

**Everything** is a machine.

Example:
A mine is considered a machine with a reservoir, say full of coal. The output ~~conveyor~~ adapter, or interface, is the hole dug into the reservoir. A machine (bulldozer) than has a switchable ~~conveyor~~ adapter that is both input and output. The bulldozer fills its (rather small) reservoir - bucket or scoop - with coal then moves to another machine in which it dumps its reservoir.

If the bulldozer dumps its coal into another machine (via the machine's input ~~conveyor~~ adapter), the machine may have multiple processing modules within it: stage 1 crushes coal, then is passed via output and input ~~conveyors~~ adapters to another processing module of the same machine to continue with stage 2 processing. Ultimately, the finished product is then output via the machine's ~~conveyor~~ adapter either into a storage hopper (another machine) or some other process (of course, it is a machine, too).

Everything is built on a very structured set of rules that are simple to follow. All of the core objects - ~~Conveyor~~ Adapter, Frame, Structure, Module, etc. - are intended to be extensible and very flexible. Modules and ~~Conveyors~~ Adapters can be enhanced for better efficiency or throughput. Multiple machines essentially establish a Factory. Connect factories together through specialized ~~Conveyors~~ Adapters (either vehicles like trucks, wagons, etc., or pipes or tubes).

### Contribution guidelines ###

* Writing tests!
* Code review
* Submit ideas to david@boarman.net

### Who do I talk to? ###

* David Boarman (david@boarman.net)