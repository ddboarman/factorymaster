﻿using System;
using System.Threading.Tasks;

using FactoryMaster.Domain.Core;

namespace FactoryMaster.Domain.API {
   /// <summary>
   /// Delegate handler when object broadcasts state change
   /// </summary>
   /// <param name="Broadcaster">object</param>
   public delegate void BroadcastStateEvent<TBroadcast>(TBroadcast OldValue, TBroadcast NewValue);

   /// <summary>
   /// Delegate handler when adapter is attached
   /// </summary>
   /// <param name="Adapter">Adpater</param>
   /// <param name="AttachedAdapter">Attached Adapter</param>
   public delegate void AdapterAttached(Adapter Adapter, Adapter AttachedAdapter);
}
