﻿using System;
using System.Collections.Generic;

using FactoryMaster.Domain.Core;
using FactoryMaster.Domain.Internal;

namespace FactoryMaster.Domain.API {
   public static class Factories {

      public static class Collections {
         public static IFrameCollection Frames(params StructureFacing[] Facings) {
            return new FrameCollection(Facings);
         }

         public static IFrameComponentCollection Components(int MaxComponentCount = 1) {
            return new FrameComponentCollection(MaxComponentCount);
         }

         public static IModuleCollection Modules(int MaxModulesCount) {
            return new ModuleCollection(MaxModulesCount);
         }
      }

      public static class Frames {
         private static int uniqueKey = 0;

         public static int NextUniqueKey {
            get { return uniqueKey++; }
         }

         public static Func<Frame> New {
            get { return ( ) => new Frame(); }
         }
      }
      
      public static class SerialNumberGenerator {
         public static Func<int, SerialNumber> New {
            get { return (UKey) => new SerialNumber(UKey); }
         }
      }

      public static class Structures {
         private static int uniqueKey = 0;

         public static int NextUniqueKey {
            get { return uniqueKey++; }
         }
      }

   }
}
