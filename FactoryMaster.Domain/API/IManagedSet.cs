﻿using System;
using System.Collections.Generic;

namespace FactoryMaster.Domain.API {
   public interface IManagedSet<TItem> : IEnumerable<TItem> {

      /// <summary>
      /// Get the current count of items in the collection
      /// </summary>
      int ItemCount { get; }

      /// <summary>
      /// Get maximum number of modules allowed in collection
      /// </summary>
      int MaxItems { get; }


      /// <summary>
      /// Add an item to the set
      /// </summary>
      /// <param name="item"></param>
      TItem AddItem(TItem item);

      /// <summary>
      /// Sets a value for the maximum number of items.
      /// </summary>
      /// <param name="MaxItemCount">Maximum number of modules allowed in the collection</param>
      void SetMaxItems(int MaxItemCount);
   }
}
