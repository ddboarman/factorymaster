﻿using System;

namespace FactoryMaster.Domain.API {
   public interface IReservoir : ILimiter {

      /// <summary>
      /// 
      /// </summary>
      double Level { get; set; }

   }
}
