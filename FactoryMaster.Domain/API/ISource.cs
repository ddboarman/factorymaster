﻿using System;

namespace FactoryMaster.Domain.API {
   public interface ISource : ILimiter {

      /// <summary>
      /// 
      /// </summary>
      double Level { get; set; }

   }
}
