﻿using System;

namespace FactoryMaster.Domain.API {
   /// <summary>
   /// Interface to limit flow or movement
   /// </summary>
   public interface ILimiter {

      /// <summary>
      /// Get the max flow or movement: units / gT
      /// </summary>
      double Max { get; }
   }
}
