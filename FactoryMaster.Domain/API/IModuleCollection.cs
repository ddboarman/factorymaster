﻿using System;
using System.Collections.Generic;

using FactoryMaster.Domain.Core;

namespace FactoryMaster.Domain.API {
   public interface IModuleCollection : IManagedSet<Module> {
   }
}
