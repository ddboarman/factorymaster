﻿using System;
using System.Collections.Generic;

using FactoryMaster.Domain.Core;

namespace FactoryMaster.Domain.API {
    public interface IFrameCollection : IEnumerable<Frame> {

       /// <summary>
       /// Get total frames count
       /// </summary>
       int Count { get; }

       /// <summary>
       /// Add a default Frame to the specified facing. If the facing has a Frame assigned, then the current
       /// Frame is the out value.
       /// </summary>
       /// <param name="Facing">The specified facing to which a Frame is assigned.</param>
       /// <param name="Frame">OUT: the assigned Frame.</param>
       /// <returns>TRUE if Frame assigned; otherwise FALSE if a Frame is already assigned.</returns>
       bool Add (StructureFacing Facing, out Frame Frame);

       /// <summary>
       /// Add a custom Frame to the specified facing. If the facing has a Frame assigned, then the current
       /// Frame is the out value.
       /// </summary>
       /// <param name="Facing">The specified facing to which a Frame is assigned.</param>
       /// <param name="Frame">The custom Frame object</param>
       /// <returns>Installed Frame</returns>
       Frame AddToFacing (StructureFacing Facing, Frame Frame);

       /// <summary>
       /// Get the facing for the specified frame.
       /// </summary>
       /// <param name="Frame">The specified frame</param>
       /// <returns>Facing: Unknown if frame not found; Otherwise: Top, Bottom, Front, Rear, Left, Right</returns>
       StructureFacing GetFrameFacing(Frame Frame);

       /// <summary>
       /// Attempt to get the Frame assigned to the specified facing
       /// </summary>
       /// <param name="Facing">The specified facing</param>
       /// <param name="Frame">OUT: The assigned frame</param>
       /// <returns>TRUE if frame exists (is installed); otherwise FALSE</returns>
       bool TryGetFacingFrame (StructureFacing Facing, out Frame Frame);

    }
}
