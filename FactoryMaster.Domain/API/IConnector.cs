﻿using System;
using FactoryMaster.Domain.Core;

namespace FactoryMaster.Domain.API {
   public interface IConnector : IBroadcast<ConnectionState> {

      ISource Source { get; }
      ConnectionState State { get; }

   }
}
