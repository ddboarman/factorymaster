﻿using System;

using FactoryMaster.Domain.Core;

namespace FactoryMaster.Domain.API {
   public interface IFrameComponentCollection : IManagedSet<FrameComponent> { }
}
