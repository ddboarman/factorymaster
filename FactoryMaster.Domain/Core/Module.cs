﻿using System;

using FactoryMaster.Domain.API;
using FactoryMaster.Domain.Internal;

namespace FactoryMaster.Domain.Core {
   public abstract class Module {

      /// <summary>
      /// 
      /// </summary>
      public string Name { get; protected set; }
      

      /// <summary>
      /// Initializes a module
      /// </summary>
      public Module ( ) { }

   }
}
