﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using FactoryMaster.Domain.API;

namespace FactoryMaster.Domain.Core {
   /// <summary>
   /// Base adapter for input or output.
   /// </summary>
   public abstract class Adapter : FrameComponent, IConnector {
      protected readonly IList<Adapter> attached = new List<Adapter>();

      /// <summary>
      /// Event raised when adapter is attached
      /// </summary>
      public event AdapterAttached Attached;

      /// <summary>
      /// Get Adapter direction: Input/Output
      /// </summary>
      public AdapterDirection Direction { get; protected set; }


      /// <summary>
      /// Construct the base Adapter.
      /// <para>Default maximum modules: 4</para>
      /// </summary>
      /// <param name="AdapterDirection">Direction: Input/Output</param>
      public Adapter(AdapterDirection AdapterDirection) : this(AdapterDirection, 4) { }

      /// <summary>
      /// Construct the base Adapter.
      /// <para>Default maximum modules: 4</para>
      /// </summary>
      /// <param name="AdapterDirection">Direction: Input/Output</param>
      /// <param name="MaxModules">Maximum modules</param>
      public Adapter(AdapterDirection AdapterDirection, int MaxModules) {
         Direction = AdapterDirection;
         modules = Factories.Collections.Modules(MaxModules);
      }


      /// <summary>
      /// 
      /// </summary>
      /// <returns></returns>
      public abstract ISource GetSource( );

      /// <summary>
      /// 
      /// </summary>
      /// <param name="adapter"></param>
      /// <returns></returns>
      public bool IsAttached(Adapter adapter) {
         return attached.Contains(adapter);
      }

      /// <summary>
      /// 
      /// </summary>
      public abstract void Tick( );


      #region IConnector Members
      /// <summary>
      /// 
      /// </summary>
      public ISource Source {
         get {
            var output = attached.First(a => a.Direction == AdapterDirection.Output);
            return output.GetSource();
         }
      }
      
      /// <summary>
      /// 
      /// </summary>
      public ConnectionState State { get; private set; }

      #endregion

      #region IBroadcast<ConnectionState> Members
      /// <summary>
      /// 
      /// </summary>
      public event BroadcastStateEvent<ConnectionState> StateChanged;

      #endregion


      /// <summary>
      /// Internal method to attach adapters
      /// </summary>
      /// <param name="Adapter">Specified adapter to attach</param>
      /// <returns>TRUE if attached; otherwise FALSE</returns>
      internal virtual bool Attach(Adapter Adapter) {
         if (Direction == Adapter.Direction) {
            // invalid

            return false;
         }

         if (attached.Contains(Adapter)) {
            // invalid

            return false;
         }

         attached.Add(Adapter);
         return attached.Contains(Adapter);
      }

      /// <summary>
      /// 
      /// </summary>
      internal void RaiseAttached(Adapter AttachedAdapter) {
         var handler = default(AdapterAttached);
         if ((handler = Attached) != null) {
            handler(this, AttachedAdapter);
         }

         CheckStateChange(attached.Any() ? ConnectionState.Connected : ConnectionState.Disconnected);
      }


      /// <summary>
      /// 
      /// </summary>
      /// <param name="connectionState"></param>
      private void CheckStateChange(ConnectionState connectionState) {
         if (State != connectionState) {
            var oldState = State;
            State = connectionState;

            Task.Factory.StartNew(() => RaiseConnectStateChanged(oldState));
         }
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="oldState"></param>
      private void RaiseConnectStateChanged(ConnectionState oldState) {
         var handler = default(BroadcastStateEvent<ConnectionState>);
         if ((handler = StateChanged) != null) {
            handler(oldState, State);
         }
      }

   }
}
