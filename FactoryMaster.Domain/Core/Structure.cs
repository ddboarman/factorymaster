﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;

using FactoryMaster.Domain.API;
using FactoryMaster.Domain.Internal;

namespace FactoryMaster.Domain.Core {
   /// <summary>
   /// Base Structure from which other objects are derived - e.g. Construct, Chassis, custom components, etc.
   /// </summary>
   public abstract class Structure : ModuleContainer {

      protected IFrameCollection frames;

      /// <summary>
      /// Gets the structure serial number
      /// </summary>
      public SerialNumber SerialNumber { get; private set; }


      /// <summary>
      /// Basic construct from which a Factory or Module is derived.
      /// </summary>
      public Structure ( ) :
         this(new SerialNumber(Factories.Structures.NextUniqueKey), StructureFacing.Top, StructureFacing.Bottom, StructureFacing.Right, StructureFacing.Left) { }

      /// <summary>
      /// Basic construct from which a Factory or Module is derived.
      /// </summary>
      /// <param name="Facings">Custom Frames install</param>
      public Structure (params StructureFacing[] Facings) :
         this(new SerialNumber(Factories.Structures.NextUniqueKey), Facings) { }

      /// <summary>
      /// Basic construct from which a Factory or Module is derived.
      /// </summary>
      /// <param name="SerialID">Structure Serial number</param>
      /// <param name="Facings">Custom Frames install</param>
      private Structure (SerialNumber SerialID, params StructureFacing[] Facings) {
         frames = Factories.Collections.Frames(Facings);
         SerialNumber = SerialID;
      }


      /// <summary>
      /// Get the Frame object installed on the specified facing.
      /// </summary>
      /// <param name="Facing">The specified structure facing</param>
      /// <returns>Frame; NULL if no Frame installed on facing</returns>
      public Frame FacingFrame (StructureFacing Facing) {
         var frame = Frame.Empty;
         frames.TryGetFacingFrame(Facing, out frame);

         return frame;
      }

      /// <summary>
      /// Get the Facing for the specified frame.
      /// </summary>
      /// <param name="Frame">The specified frame</param>
      /// <returns>Facing: Unknown if frame not found; Otherwise: Top, Bottom, Front, Rear, Left, Right</returns>
      public StructureFacing FrameFacing (Frame Frame) {
         return frames.GetFrameFacing(Frame);
      }

      /// <summary>
      /// Installs a default frame in the specified cube facing.
      /// </summary>
      /// <param name="Facing">The specified cube facing.</param>
      /// <returns>TRUE if the frame was installed or a frame was already installed.</returns>
      public bool InstallFrame (StructureFacing Facing) {
         var frame = default(Frame);
         var isInstalled = frames.Add(Facing, out frame);

         return isInstalled || frame != null;
      }

      /// <summary>
      /// Creates a Module and adds to the modules collection
      /// </summary>
      /// <typeparam name="TFrame">TFrame : Frame</typeparam>
      /// <param name="GetFrame">Function to instantiate frame</param>
      /// <returns>TFrame</returns>
      public TFrame InstallFrame<TFrame> (StructureFacing Facing, Expression<Func<TFrame>> GetFrame) where TFrame : Frame {
         // we have the expression which can be 'dissected' for other purposes later
         var getFrame = GetFrame.Compile( );

         return (TFrame)frames.AddToFacing(Facing, getFrame( ));
      }

      /// <summary>
      /// 
      /// </summary>
      public abstract void Tick( );

   }
}
