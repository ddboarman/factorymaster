﻿using System;

namespace FactoryMaster.Domain.Core {
   public abstract class ConfigurableModule<TModule> : Module {
      protected TModule Module { private get; set; }

      public abstract TModule Configure<TValue> (Func<TModule, TValue> Set);
      
   }
}
