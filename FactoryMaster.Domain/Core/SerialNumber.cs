﻿using System;
using System.Linq;
using System.Text;

namespace FactoryMaster.Domain.Core {
   public struct SerialNumber {
      private static readonly Lazy<SerialNumber> empty = new Lazy<SerialNumber>(() => new SerialNumber(new byte[16]));

      private readonly byte[] serialNumber;
      private readonly int uniqueKey;


      /// <summary>
      /// Get an empty serial number object
      /// </summary>
      public static SerialNumber Empty {
         get { return empty.Value; }
      }


      /// <summary>
      /// Intialize a serial number
      /// </summary>
      /// <param name="UniqueKey">Unique Key ID</param>
      public SerialNumber(int UniqueKey) {
         serialNumber = Guid.NewGuid().ToString()
            .Replace("-", string.Empty)
            .ToUpper()
            .Take(16)
            .Select(c => (byte)c)
            .ToArray();
         uniqueKey = UniqueKey;
      }

      /// <summary>
      /// Initializer for an empty serial number object
      /// </summary>
      /// <param name="serNum">byte[16]</param>
      private SerialNumber(byte[] serNum) {
         serialNumber = serNum;
         uniqueKey = 0;
      }


      #region Object overrides      
      public override string ToString() {
         return String.Format("{0}-{1}", Encoding.UTF8.GetString(serialNumber), uniqueKey);
      }

      public override bool Equals(object obj) {
         var serNum = Empty;

         if (!(obj is SerialNumber)) {
            return false;
         }
         else {
            serNum = (SerialNumber)obj;
         }

         return this == serNum;
      }
      #endregion


      #region operator overloads
      public static bool operator ==(SerialNumber serNum1, SerialNumber serNum2) {
         return serNum1.ToString().Equals(serNum2.ToString(), StringComparison.InvariantCulture);
      }

      public static bool operator !=(SerialNumber serNum1, SerialNumber serNum2) {
         return !serNum1.ToString().Equals(serNum2.ToString(), StringComparison.InvariantCulture);
      }
      #endregion

   }
}
