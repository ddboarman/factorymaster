﻿using System;

namespace FactoryMaster.Domain.Core {
   public enum ResourceState {
      Unknown,
      Liquid,
      Gas,
      Plasma,
      Solid
   }
}
