﻿using System;

namespace FactoryMaster.Domain.Core {
   public enum AdapterDirection {
      Unknown = -1,
      Input = 1,
      Output,
      Virtual
   }
}
