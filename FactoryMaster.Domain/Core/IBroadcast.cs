﻿using System;
//
using FactoryMaster.Domain.API;

namespace FactoryMaster.Domain.Core {
   public interface IBroadcast<TBroadcast> {

      event BroadcastStateEvent<TBroadcast> StateChanged;

   }
}
