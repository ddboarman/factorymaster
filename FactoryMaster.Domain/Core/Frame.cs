﻿using System;
using System.Linq;
using System.Collections.Generic;

using FactoryMaster.Domain.API;
using FactoryMaster.Domain.Internal;

namespace FactoryMaster.Domain.Core {
   /// <summary>
   /// Basic functional throughput device; agnostic of input or output.
   /// <para>Required to install an Component.</para>
   /// </summary>
   public class Frame {
      private static readonly Lazy<Frame> empty = new Lazy<Frame>(() => { return new Frame(SerialNumber.Empty, 0); });

      private readonly IFrameComponentCollection components;

      internal FrameComponentCollection Components {
         get { return (FrameComponentCollection)components; }
      }

      /// <summary>
      /// Get an empty frame object
      /// </summary>
      public static Frame Empty {
         get { return empty.Value; }
      }

      /// <summary>
      /// Get the current Frame serial number
      /// </summary>
      public SerialNumber SerialNumber { get; private set; }

      /// <summary>
      /// Get the current Frame maximum interfaces
      /// </summary>
      public int MaxComponents {
         get { return components.MaxItems; }
      }


      /// <summary>
      /// Basic functional throughput device; agnostic of input or output.
      /// <para>Required to install an Component.</para>
      /// </summary>
      /// <param name="MaxComponentCount">OPTIONAL: Maximum allowed Components
      /// <para>DEFAULT: 1</para>
      /// </param>
      public Frame(int MaxComponentCount = 1) :
         this(new SerialNumber(Factories.Frames.NextUniqueKey), MaxComponentCount) { }

      /// <summary>
      /// Basic functional throughput device; agnostic of input or output.
      /// <para>Required to install an Component.</para>
      /// </summary>
      /// <param name="SerialID">Serial number</param>
      /// <param name="MaxComponentCount">OPTIONAL: Maximum allowed Components
      /// <para>DEFAULT: 1</para>
      /// </param>
      private Frame(SerialNumber SerialID, int MaxComponentCount = 1) {
         components = Factories.Collections.Components(MaxComponentCount);
         SerialNumber = SerialID;
      }


      /// <summary>
      /// 
      /// </summary>
      /// <typeparam name="TComponent"></typeparam>
      /// <param name="GetComponent"></param>
      /// <returns></returns>
      public TComponent Create<TComponent>(Func<TComponent> GetComponent) where TComponent : FrameComponent {
         var component = GetComponent();
         if (!Install(component)) {
            return default(TComponent);
         }

         return component;
      }

      /// <summary>
      /// 
      /// </summary>
      /// <typeparam name="TComponent"></typeparam>
      /// <returns></returns>
      public TComponent[] GetComponents<TComponent> ( ) {
         return components
            .Where(c => c is TComponent)
            .OfType<TComponent>()
            .ToArray( );
      }

      /// <summary>
      /// Installs the specified component in the current frame.
      /// </summary>
      /// <param name="Component">The specified component</param>
      /// <returns>TRUE if component installed; otherwise FALSE</returns>
      public bool Install(FrameComponent Component) {
         var component = default(FrameComponent);
         if ((component = components.AddItem(Component)) != null) {
            component.Frame = this;
         }

         return component != null;
      }

      /// <summary>
      /// Installs the specified component in the current frame.
      /// </summary>
      /// <typeparam name="TComponent"></typeparam>
      /// <param name="FrameComponent"></param>
      /// <param name="Component"></param>
      /// <returns>TRUE if component installed; otherwise FALSE</returns>
      public bool Install<TComponent> (FrameComponent FrameComponent, out TComponent Component) where TComponent : FrameComponent {
         bool isInstalled = Install(FrameComponent);
         Component = (TComponent)FrameComponent;

         return isInstalled;
      }

      /// <summary>
      /// Sets a limit on the number of Components that a Frame may contain.
      /// </summary>
      /// <param name="MaxComponentCount">Maximum allowed Components</param>
      /// <returns>FLUENT: current Frame</returns>
      public Frame SetMaxComponents(int MaxComponentCount) {
         if (this == Frame.Empty) {
            // invalid
            return null;
         }

         components.SetMaxItems(MaxComponentCount);

         return this;
      }


      #region object overrides
      /// <summary>
      /// Determines if two Frame objects are identical: check serial numbers
      /// </summary>
      /// <param name="obj"></param>
      /// <returns></returns>
      public override bool Equals(object obj) {
         var frame = obj as Frame;
         if (obj == null || frame == null) {
            return false;
         }

         return SerialNumber.Equals(frame.SerialNumber);
      }
      #endregion


      #region operator overloads
      public static bool operator ==(Frame frame1, Frame frame2) {
         return (Object.Equals(frame1, null) != Object.Equals(frame2, null)) ? 
            false :
            frame1.SerialNumber == frame2.SerialNumber;
      }

      public static bool operator !=(Frame frame1, Frame frame2) {
         return (Object.Equals(frame1, null) != Object.Equals(frame2, null)) ? 
            true : 
            frame1.SerialNumber != frame2.SerialNumber;
      }
      #endregion

   }
}
