﻿using System;
using System.Linq.Expressions;
//
using FactoryMaster.Domain.API;

namespace FactoryMaster.Domain.Core {
   public abstract class ModuleContainer {

      protected IModuleCollection modules;


      /// <summary>
      /// Get total number of module slots
      /// </summary>
      public int MaxModules {
         get { return modules.MaxItems; }
      }

      /// <summary>
      /// Get number of modules installed in construct
      /// </summary>
      public int ModuleCount {
         get { return modules.ItemCount; }
      }


      /// <summary>
      /// 
      /// </summary>
      public ModuleContainer( ) {
         modules = Factories.Collections.Modules(4);
      }


      /// <summary>
      /// Creates a Module and adds to the modules collection
      /// </summary>
      /// <typeparam name="TModule">TModule : Module</typeparam>
      /// <param name="GetModule">Function to instantiate module</param>
      /// <returns>TModule</returns>
      public TModule CreateModule<TModule> (Expression<Func<TModule>> GetModule) where TModule : Module {
         // we have the expression which can be 'dissected' for other purposes later
         var getModule = GetModule.Compile( );
         TModule module = getModule();

         return (TModule)modules.AddItem(module);
      }

      /// <summary>
      /// Get module from collection
      /// </summary>
      /// <typeparam name="TModule">TModule : Module</typeparam>
      /// <returns>TModule</returns>
      public TModule GetModule<TModule> ( ) where TModule : Module {
         var iterator = modules.GetEnumerator( );
         var module = default(TModule);

         while (iterator.MoveNext( )) {
            if (iterator.Current is TModule) {
               module = (TModule)iterator.Current;

               break;
            }
         }

         return module;
      }

      /// <summary>
      /// Sets the maximum modules allowed
      /// </summary>
      /// <param name="MaxModuleCount"></param>
      public void SetMaxModules (int MaxModuleCount) {
         modules.SetMaxItems(MaxModuleCount);
      }

   }
}
