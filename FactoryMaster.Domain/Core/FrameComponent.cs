﻿using System;

namespace FactoryMaster.Domain.Core {
   public abstract class FrameComponent : ModuleContainer {

      /// <summary>
      /// Get the frame component's owner Frame
      /// </summary>
      public Frame Frame { get; internal set; }

   }
}
