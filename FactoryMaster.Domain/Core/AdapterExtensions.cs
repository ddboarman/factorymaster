﻿using System;
using FactoryMaster.Domain.API;

namespace FactoryMaster.Domain.Core {
   public static class AdapterExtensions {

      /// <summary>
      /// Attach another specified adapter to the current adapter.
      /// <para>
      /// An OUTPUT adapter can attach to: INPUT, MULTI
      /// </para>
      /// <para>
      /// An INPUT adapter can attach to: OUTPUT, MULTI
      /// </para>
      /// </summary>
      /// <param name="Adapter">The specified adapter to attach</param>
      /// <returns>TRUE if attached; otherwise FALSE</returns>
      public static bool AttachAdapter(this Adapter Source, Adapter Destination) {
         var isAttached = false;
         if (Source.Attach(Destination)) {
            isAttached = Destination.Attach(Source);
         }

         Source.RaiseAttached(Destination);
         Destination.RaiseAttached(Source);

         return isAttached;
      }
   }
}
