﻿using System;

namespace FactoryMaster.Domain.Core {
   public enum StructureFacing {
      Unknown = -1,
      Top = 1,
      Bottom,
      Rear,
      Front,
      Right,
      Left,
      Virtual
   }
}
