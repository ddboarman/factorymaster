﻿using System;
using System.Linq.Expressions;

namespace FactoryMaster.Domain.Core {
   public static class StructureExtensions {

      public static TSource Configure<TSource, TConfig, TResult> (this TSource Structure, Func<TSource, TConfig> ConfigObj, Func<TConfig, TResult> Configure) where TSource : Structure {
         try {
            TConfig configObj = ConfigObj(Structure);
            Configure(configObj);
         }
         catch (Exception ex) {
            
            throw;
         }

         return Structure;
      }

   }
}
