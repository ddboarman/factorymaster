﻿using System;

namespace FactoryMaster.Domain.Core {
   public abstract class Resource {

      public ResourceState State { get; protected set; }

   }
}
