﻿using System;
using System.Linq;

namespace FactoryMaster.Domain.Core {
    public static class FrameExtensions {

        public static Adapter[] Inputs(this Frame Frame) {
           return Frame.Components
              .OfType<Adapter>()
              .Where(a => a.Direction == AdapterDirection.Input)
              .ToArray();
        }

        public static Adapter[] Outputs(this Frame Frame) {
           return Frame.Components
              .OfType<Adapter>()
              .Where(a => a.Direction == AdapterDirection.Output)
              .ToArray();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TComponent"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="Component"></param>
        /// <param name="Set"></param>
        /// <returns></returns>
        public static TComponent Configure<TComponent, TValue> (this TComponent Component, Func<TComponent, TValue> Set) where TComponent : FrameComponent {
           Set(Component);

           return Component;
        }

    }
}
