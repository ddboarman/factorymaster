﻿using System;

namespace FactoryMaster.Domain.Core {
   public enum ConnectionState {
      Unknown = 0,
      Connected = 1,
      Disconnected = 2
   }
}
