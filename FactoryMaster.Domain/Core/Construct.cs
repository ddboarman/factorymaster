﻿using System;

using FactoryMaster.Domain.API;

namespace FactoryMaster.Domain.Core {
   /// <summary>
   /// Base Construct from which large-scale components are derived - e.g. Lake, Mine, or other near-infinite resources.
   /// </summary>
   public abstract class Construct : Structure {
      
      /// <summary>
      /// Initializer for base Construct.
      /// </summary>
      /// <param name="Facings">Default facings with which to initialize the construct</param>
      public Construct (StructureFacing[] Facings) : this(Facings, 1) { }

      /// <summary>
      /// Initializer for base Construct.
      /// </summary>
      /// <param name="Facings">Default facings with which to initialize the construct</param>
      /// <param name="MaxModulesCount">Set default max modules count</param>
      protected Construct (StructureFacing[] Facings, int MaxModulesCount)
         : base(Facings) {
            modules = Factories.Collections.Modules(MaxModulesCount);
      }

   }
}
