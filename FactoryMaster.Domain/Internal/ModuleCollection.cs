﻿using System;

using FactoryMaster.Domain.API;
using FactoryMaster.Domain.Core;

namespace FactoryMaster.Domain.Internal {
   internal class ModuleCollection : ManagedSet<Module>, IModuleCollection {

      public ModuleCollection(int MaxModulesCount) : base(MaxModulesCount) { }

   }
}
