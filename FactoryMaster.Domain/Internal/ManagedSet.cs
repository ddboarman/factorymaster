﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
//
using FactoryMaster.Domain.API;

namespace FactoryMaster.Domain.Internal {
   internal abstract class ManagedSet<TItem> : IManagedSet<TItem>, IEnumerable<TItem>, IList<TItem> {
      protected TItem[] items;


      protected ManagedSet(int MaxModulesCount) {
         // initialize Module array
         items = new TItem[MaxModulesCount];
      }


      #region IManagedSet<TItem> Members

      /// <summary>
      /// Get count of items in the set
      /// </summary>
      public int ItemCount {
         get { return Count; }
      }

      /// <summary>
      /// Get maximum number of items allowed in set
      /// </summary>
      public int MaxItems {
         get { return items.Length; }
      }

      /// <summary>
      /// Adds a specified item to the first available empty position in the set.
      /// </summary>
      /// <param name="item">The specified item</param>
      /// <returns>Item if added; otherwise NULL</returns>
      public TItem AddItem(TItem item) {
         if (!items.Any(m => m == null)) {
            // invalid - no available positions for an item
         }

         var idx = NextEmptyLocation();
         if (idx.HasValue) {
            Insert(idx.Value, item);
            return item;
         }
         else {
            return default(TItem);
         }
      }

      /// <summary>
      /// Sets a value for the maximum number of modules. May have
      /// undesired consequences if reducing the size where a module
      /// may be installed.
      /// </summary>
      /// <param name="MaxItemCount">Maximum number of modules allowed in the collection</param>
      public void SetMaxItems(int MaxItemCount) {
         var array = new TItem[MaxItemCount];
         CopyTo(array, 0);

         items = array;
      }
      
      #endregion

      #region IEnumerable<TItem>, IList<TItem>, ICollection<TItem> Members
      /// <summary>
      /// Get or set an item at the specified index.
      /// </summary>
      /// <param name="index">Specified index</param>
      /// <returns>Item</returns>
      public TItem this[int index] {
         get { return items[index]; }
         set { Insert(index, value); }
      }

      /// <summary>
      /// Copies the set to an array beginning from the specified array index.
      /// </summary>
      /// <param name="array">Source array</param>
      /// <param name="arrayIndex">Beginning index in source array</param>
      public void CopyTo(TItem[] array, int arrayIndex) {
         if (array.Length < items.Length) {
            // invalid - must be at least same length
         }

         Array.Copy(items, arrayIndex, array, 0, items.Length);
      }

      /// <summary>
      /// Get count of items in the set
      /// </summary>
      public int Count {
         get {
            return items
               .Where(i => i != null)
               .Count();
         }
      }

      /// <summary>
      /// Gets a value to determine if the set is readonly.
      /// </summary>
      public bool IsReadOnly {
         get { return items.IsReadOnly; }
      }

      /// <summary>
      /// Adds a specified item to the first available empty position in the set.
      /// </summary>
      /// <param name="item">Item to add</param>
      public void Add(TItem item) {
         AddItem(item);
      }

      /// <summary>
      /// Clears the set.
      /// </summary>
      public void Clear() {
         for (int idx = 0; idx < items.Length; idx++) {
            RemoveAt(idx);
         }
      }

      /// <summary>
      /// Searches for the specified item.
      /// </summary>
      /// <param name="item">The specified item</param>
      /// <returns>TRUE if item found; otherwise FALSE</returns>
      public bool Contains(TItem item) {
         return IndexOf(item) != -1;
      }

      /// <summary>
      /// Returns an enumerator that iterates through the collection.
      /// </summary>
      /// <returns>An enumerator that can be used to iterate through the set</returns>
      public IEnumerator<TItem> GetEnumerator() {
         return items
            .OfType<TItem>()
            .GetEnumerator();
      }

      /// <summary>
      /// Returns an enumerator that iterates through the collection.
      /// </summary>
      /// <returns>An enumerator that can be used to iterate through the set</returns>
      IEnumerator IEnumerable.GetEnumerator() {
         return items.GetEnumerator();
      }

      /// <summary>
      /// Searches for the specified item.
      /// </summary>
      /// <param name="item">The specified item</param>
      /// <returns>Index of the item if found; otherwise -1</returns>
      public int IndexOf(TItem item) {
         return Array.IndexOf(items, item);
      }

      /// <summary>
      /// Inserts the specified item in the set if the position is empty.
      /// </summary>
      /// <param name="index">Index at which to insert item.</param>
      /// <param name="item">The specified item to insert</param>
      public void Insert(int index, TItem item) {
         if (index < 0 || index > items.Length) {
            // invalid
         }

         if (items[index] != null) {
            // invalid
         }

         items[index] = item;
      }

      /// <summary>
      /// Remove the specified item.
      /// </summary>
      /// <param name="item">The specified item to be removed</param>
      /// <returns>TRUE if removed; otherwise FALSE</returns>
      public bool Remove(TItem item) {
         var idx = IndexOf(item);

         if (idx != -1) {
            RemoveAt(idx);
         }

         return idx != -1;
      }

      /// <summary>
      /// Remove an item at the specified index
      /// </summary>
      /// <param name="index">The specified index</param>
      public void RemoveAt(int index) {
         if (index < 0 || index > items.Length) {
            // invalid
         }

         items[index] = default(TItem);
      }

      #endregion


      protected int? NextEmptyLocation() {
         return items
            .Where(c => c == null)
            .Select((c, i) => {
               if (c == null) {
                  return (int?)i;
               }
               else {
                  return (int)-1;
               }
            })
            .ToList()
            .FirstOrDefault(i => i.Value != -1);
      }
   }
}
