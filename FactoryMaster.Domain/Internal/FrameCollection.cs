﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using FactoryMaster.Domain.API;
using FactoryMaster.Domain.Core;

namespace FactoryMaster.Domain.Internal {
   /// <summary>
   /// Default FrameCollection.
   /// </summary>
   internal class FrameCollection : IFrameCollection, IEnumerable<Frame>, IDictionary<StructureFacing, Frame> {
      private readonly Dictionary<StructureFacing, Frame> frames = new Dictionary<StructureFacing, Frame>( );


      /// <summary>
      /// Initializes a default FrameCollection.
      /// </summary>
      /// <param name="Facings">OPTIONAL: StructureFacing</param>
      internal FrameCollection (params StructureFacing[] Facings) {
         for (int idx = 0 ; idx < Facings.Length ; idx++) {
            frames.Add(Facings[idx], new Frame( ));
         }
      }


      #region IFrameCollection Members
      /// <summary>
      /// Add a Frame to the specified facing. If the facing has a Frame assigned, then the current
      /// Frame is the out value.
      /// </summary>
      /// <param name="Facing">The specified facing to which a Frame is assigned.</param>
      /// <param name="Frame">OUT: the assigned Frame.</param>
      /// <returns>TRUE if Frame assigned; otherwise FALSE if a Frame is already assigned.</returns>
      public bool Add (StructureFacing Facing, out Frame Frame) {
         if (frames.TryGetValue(Facing, out Frame)) {
            return false;
         }

         return (Frame = (frames[Facing] = new Frame( ))) != null;
      }

      /// <summary>
      /// Add a custom Frame to the specified facing. If the facing has a Frame assigned, then the current
      /// Frame is the out value.
      /// </summary>
      /// <param name="Facing">The specified facing to which a Frame is assigned.</param>
      /// <param name="Frame">The custom Frame object</param>
      /// <returns>Installed Frame</returns>
      public Frame AddToFacing (StructureFacing Facing, Frame Frame) {
         var frame = default(Frame);
         if (!frames.TryGetValue(Facing, out frame)) {
            // invalid - facing already has a frame installed
            return null;
         }

         return (frames[Facing] = Frame);
      }

      /// <summary>
      /// Get the facing for the specified frame.
      /// </summary>
      /// <param name="Frame">The specified frame</param>
      /// <returns>Facing: Unknown if frame not found; Otherwise: Top, Bottom, Front, Rear, Left, Right</returns>
      public StructureFacing GetFrameFacing (Frame Frame) {
         var facings = frames
            .Where(kv => kv.Value == Frame)
            .Select(kv => kv.Key)
            .ToList( );

         return facings.FirstOrDefault( ) == 0 ? StructureFacing.Unknown : facings.First( );
      }

      /// <summary>
      /// Attempt to get the Frame assigned to the specified facing
      /// </summary>
      /// <param name="Facing">The specified facing</param>
      /// <param name="Frame">OUT: The assigned frame</param>
      /// <returns>TRUE if frame exists (is installed); otherwise FALSE</returns>
      public bool TryGetFacingFrame (StructureFacing Facing, out Frame Frame) {
         if (!frames.TryGetValue(Facing, out Frame)) {
            Frame = Frame.Empty;
         }

         return Frame != Frame.Empty;
      }

      #endregion

      #region IEnumerable<Frame>, IDictionary<StructureFacing, Frame> Members
      /// <summary>
      /// 
      /// </summary>
      /// <param name="key"></param>
      /// <returns></returns>
      public Frame this[StructureFacing key] {
         get {
            throw new NotImplementedException( );
         }
         set {
            throw new NotImplementedException( );
         }
      }

      /// <summary>
      /// Get total number of frames in the collection.
      /// </summary>
      public int Count {
         get {
            return frames.Values
               .Where(v => v != null)
               .Count( );
         }
      }

      public bool IsReadOnly {
         get { throw new NotImplementedException( ); }
      }

      public ICollection<StructureFacing> Keys {
         get { throw new NotImplementedException( ); }
      }

      public ICollection<Frame> Values {
         get { throw new NotImplementedException( ); }
      }

      public void Add (StructureFacing key, Frame value) {
         var frame = default(Frame);

         if (!TryGetValue(key, out frame)) {
            frames[key] = value;
         }
      }

      public void Add (KeyValuePair<StructureFacing, Frame> item) {
         Add(item.Key, item.Value);
      }

      public void Clear ( ) {
         throw new NotImplementedException( );
      }

      public bool Contains (KeyValuePair<StructureFacing, Frame> item) {
         throw new NotImplementedException( );
      }

      public bool ContainsKey (StructureFacing key) {
         throw new NotImplementedException( );
      }

      public void CopyTo (KeyValuePair<StructureFacing, Frame>[] array, int arrayIndex) {
         throw new NotImplementedException( );
      }
      public bool Remove (StructureFacing key) {
         throw new NotImplementedException( );
      }

      public bool Remove (KeyValuePair<StructureFacing, Frame> item) {
         throw new NotImplementedException( );
      }

      public bool TryGetValue (StructureFacing key, out Frame value) {
         //value = default(Frame);
         return frames.TryGetValue(key, out value);
      }

      public IEnumerator<Frame> GetEnumerator ( ) {
         return frames.Values.GetEnumerator();
      }

      IEnumerator IEnumerable.GetEnumerator( ) {
         return frames.Values.GetEnumerator();
      }

      IEnumerator<KeyValuePair<StructureFacing, Frame>> IEnumerable<KeyValuePair<StructureFacing, Frame>>.GetEnumerator ( ) {
         throw new NotImplementedException( );
      }
      #endregion

   }
}
