﻿using System;

using FactoryMaster.Domain.API;
using FactoryMaster.Domain.Core;

namespace FactoryMaster.Domain.Internal {
   /// <summary>
   /// Default FrameComponentCollection
   /// </summary>
   internal class FrameComponentCollection : ManagedSet<FrameComponent>, IFrameComponentCollection {

      /// <summary>
      /// Initializes a default FrameComponentCollection
      /// </summary>
      /// <param name="MaxFrameComponents">Maximum components in default collection</param>
      internal FrameComponentCollection(int MaxFrameComponents) : base(MaxFrameComponents) { }

   }
}
